"use strict";

describe("testing checkout", () => {
    const Checkout = require("../src/Checkout");

    let checkout = null;

    beforeEach(() => {
        checkout = new Checkout();
    });

    afterEach(() => {
        checkout = null;
    });

    test("can calculate total ", () => {
        checkout.addItemPrice("item", 20);
        checkout.addItem("item");
        expect(checkout.calculateTotal()).toBe(20);
    });

    test("can calculate total for multiple items ", () => {
        checkout.addItemPrice("ipod", 20);
        checkout.addItem("ipod");
        checkout.addItemPrice("ipad", 30);
        checkout.addItem("ipad");
        expect(checkout.calculateTotal()).toBe(50);
    });

    test("can add discount rule ", () => {
        expect(checkout.addDiscountRule("ipod", 2, 20)).toBeUndefined();
    });

    test.skip("can apply discount rule ", () => {
        checkout.addItemPrice("ipod", 20);
        checkout.addDiscountRule("ipod", 2, 10);
        checkout.addItem("ipod");
        checkout.addItem("ipod");
        checkout.addItem("ipod");
        expect(checkout.calculateTotal()).toBeLessThan(60);
        expect(checkout.calculateTotal()).toBe(40);
    });

    test("throws error for item added without price ", () => {
        //Note: You must wrap the code in a function, otherwise the error will not be caught and the assertion will fail.
        expect(() => {
            checkout.addItem("surfacepro");
        }).toThrowError(/price$/);
    });
});
