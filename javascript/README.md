# notes

## links
[examples](http://zetcode.com/javascript/jest/)

## phases in testing
1. red
2. green
3. refactor

---

## todo
1. can create an instance of the checkout class √
2. can add an item price √
3. can add an item
4. can calculate current total
5. can add multiple items and get correct total
6. can add discount rules
7. can apply discount rules to total
8. can throw exception for item added without price

---

## notes
- `npx jest --watch -- checkout` to watch folder and run pytest on change
- `jest --watch --verbose=false` to show and `jest --watch --silent` to hide **console.*** output
