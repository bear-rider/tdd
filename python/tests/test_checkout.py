import pytest

from src.checkout import Checkout


@pytest.fixture()
def checkout():
    checkout = Checkout()
    return checkout

def test_canCalculateTotal(checkout):
    checkout.add_item_price("item", 20)
    checkout.add_item("item")
    assert checkout.calculate_total() == 20

def test_canCalculateTotalForMultipleItems(checkout):
    checkout.add_item_price("ipod", 20.0)
    checkout.add_item("ipod")
    checkout.add_item_price("ipad", 30.0)
    checkout.add_item("ipad")
    assert checkout.calculate_total() == 50.0

@pytest.mark.skip
def test_canAddDiscountRule(checkout):
    checkout.add_item_price("ipod", 20)
    checkout.add_discount_rule("ipod", 2, 10)
    checkout.add_item("ipod")
    checkout.add_item("ipod")
    checkout.add_item("ipod")
    assert checkout.calculate_total() < 60
    assert checkout.calculate_total() == 40

def test_raiseExceptionForItemWithoutPrice(checkout):
    with pytest.raises(Exception,  match=r"price$"):
        checkout.add_item("surfacepro")