"use strict";

module.exports = class Checkout {
    constructor() {
        this.prices = {};
        this.discounts = {};
        this.items = {};
        this.Discount = class Discount {
            constructor(items, price) {
                this.items = items;
                this.price = price;
            }
        };
    }

    addItemPrice(item, price) {
        this.prices[item] = price;
    }
    addItem(item) {
        if (!this.prices[item]) {
            throw new Error("an item cannot be added without a price");
        }
        if (item in this.items) {
            this.items[item] = this.items[item] + 1;
        } else {
            this.items[item] = 1;
        }
    }

    addDiscountRule(item, minItems, discountedPrice) {
        const discount = new this.Discount(minItems, discountedPrice);
        this.discounts[item] = discount;
    }

    calculateTotal() {
        let total = 0.0;
        for (const item in this.items) {
            let cost = this.prices[item];
            let count = this.items[item];
            total = total + cost * count;
        }
        return total;
    }
};
