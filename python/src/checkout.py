from functools import reduce

class Checkout:
    class Discount:
        def __init__(self, min_items, price):
            self.min_items = min_items
            self.price = price

    def __init__(self):
        self.prices = {}
        self.discounts = {}
        self.items = {}

    def add_item_price(self, item, price):
        self.prices[item] = price

    def add_item(self, item):
        if item not in self.prices:
            raise Exception("an item cannot be added without a price")

        if item in self.items:
            self.items[item] = self.items[item] + 1
        else:
            self.items[item] = 1

    def add_discount_rule(self, item, min_items, disounted_price):
        discount = self.Discount(min_items, disounted_price)
        self.discounts[item] = discount

    def calculate_total(self):
        total = 0.0
        for item, count in self.items.items():
            if item in self.discounts:
                discount = self.discounts[item]
                if count >= discount.min_items:
                    number_of_discounts = count/discount.min_items
                    total = total + number_of_discounts * discount.price
                    remaining = count % discount.min_items
                    total = total + remaining * self.prices[item]
                else:
                    total = total + self.prices[item] * count
            else:
                total = total + (self.prices[item] * count)
        return total